#!/bin/sh
## Pohjaton configuration file

# If you wish to change these settings, the best place to put your config file
# so it doesn't get overwritten on software updates would be under $CONFDIR.
# If you wish to use your own template, the best place to put it would be under
# $DATADIR. More on these below.
#
# N.B. These directories are not created automatically by the tool.
# You can create them manually or with the tool commands
#    pohjaton conf
# and
#    pohjaton data

## Directories
# $SELFDIR will point to the tool repository's root directory, e.g.
#    ~/src/pohjaton/
# $CONFDIR will point to our subdirectory under $XDG_CONFIG_HOME, usually
#    ~/.config/org.meklu.pohjaton/
# $DATADIR will point to our subdirectory under $XDG_DATA_HOME, usually
#    ~/.local/share/org.meklu.pohjaton/

## Replacements

# By default we use the user's real name retrieved via git config
AUTHOR="$(git config user.name)"

# The default date format looks a little something like the following:
#    1970-01-22
DATEFORMAT="%Y-%m-%d"

# We provide a helper function (`numextract`) to try and extract numbers from
# the given filename. It works by taking the filename and grabbing all the
# characters before the file extension that come after a number. The helper
# also takes an optional fallback argument for when no number is found - the
# default fallback string is simply N.
# Example:
#    invocation: "Exercise $(numextract FIXME)"
#    filename  : vko_3b.tex       output: Exercise 3b
#    filename  : assignment.tex   output: Exercise FIXME
# Adjust as necessary.
TITLE="Harjoitus $(numextract N)"

## Files

# By default we use the provided template which resides in the repo root
#    ${SELFDIR}/pohja.tex
# If you want to use your own template, you should uncomment and adjust the
# line below as needed
#TEMPLFIL="${DATADIR}/custom.tex"

## TBD

# List of default options
#OPTIONS="noauthor"
