# Pohjaton

Tired of constantly copying your favourite template for each and every LaTeX
document that you write? Drowning in endless revisions of said template? Fear
not, for Pohjaton is here for you!

This is a nice little LaTeX template tool of mine that takes a template,
replaces some strings and outputs a nice starting point of a file. Very simple,
very usable.

You can also customise the string replacements via slapping a file at
`$XDG_CONFIG_HOME/org.meklu.pohjaton/conf.sh`. A sample file is provided in the
repository root here with more information enveloped within.

You can also use a custom template. The recommended path for that is under
`$XDG_DATA_HOME/org.meklu.pohjaton/`, as documented in the default
configuration file.

## Usage

You can symbolically link this tool to some place in your `PATH`, e.g. `~/bin`
and the rest should be handled nicely. The program will print out a usage
statement like the following if deemed to be invoked incorrectly or if the help
action is specified:

```
USAGE:
        ./pohjaton <outfile.tex> [<opt>, ...]
        ./pohjaton conf
                Create config directory with default config
        ./pohjaton data
                Create empty data directory
        ./pohjaton help
                Print this usage statement
```

The program expects you to give a first argument as the now-to-be-created file
and expects it to have a `.tex` filename suffix. You can use either a relative
or an absolute path.

## TBD

More sophisticated options, per-directory options, yadda yadda. Maybe with a
capital M.
